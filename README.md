# GCH3515-Phénomènes d'échanges

## Table des matières

* [Description](#description)
  * [Énergie](#energie)
  * [Mouvement](#mouvement)
  * [Masse](#masse)
* [Notes de cours](#notes-de-cours)
* [Exercices](#exercices)
  * [Recette](#recette)
* [TD](#td)
* [Feuille de notes](#feuille-de-notes)

## Description

Le cours de phénomènes d'échanges permet d'observer les échanges
d'[énergie](#energie), de [mouvement](#mouvement) et de [masse](#masse)
en étudiant l'ensemble du système.

### Energie

**Loi de Fourier :**  
Unidimentionnelle en x :
```math
q_x = -k \tfrac{dT}{dx}  \quad ou \quad \tfrac{Q}{A} = -k \tfrac{dT}{dx}
```
Unités (note : $`\Delta`$ °C = $`\Delta`$ K) :
```math
[\tfrac{W}{m^2}] = [\tfrac{W}{m \cdot K}]\cdot[\tfrac{K}{m}]
```
Unités SI : 
```math
[\tfrac{kg}{s^3}] = [\tfrac{kg \cdot m}{s^3 \cdot K}]\cdot[\tfrac{K}{m}]
```
Forme générale :
```math
q = -k \Delta T
```

**Loi de refroidissement de Newton :**  
Unidimentionnelle en x :
```math
q_x = h(T_0 - T_a)
```
Avec la loi de Fourier :
```math
-k \tfrac{dT}{dx}= h(T_0 - T_a)
```
Unités (note : $`\Delta`$ °C = $`\Delta`$ K) :
```math
[\tfrac{W}{m^2 \cdot K}] = [\tfrac{W}{m^2 \cdot K}]\cdot[K] 
```
Unités SI :
```math
[\tfrac{kg}{s^3}] = [\tfrac{kg}{s^3 \cdot K}]\cdot[K]
```
Forme générale :
```math
q_0 = h(T_0 - T_{fluide})
```

### Mouvement

**Loi de cisaillement de Newton :**  
Unidimmentionnelle en x :
```math
\tau_{yx} = - \mu \tfrac{dV_x}{dy}
```
Unités :
```math
[\tfrac{W}{m^2 \cdot K}] = [ \tfrac{W}{m^2 \cdot K} ]\cdot[K]
```
Forme générale :
```math
\tau = - \mu (\nabla v + (\nabla v)^t) + (\frac{2}{3} \mu - \kappa)(\nabla \cdot v)\delta
```

### Masse

**Premnière loi de Fick :**  
Unidimmentionnelle en x :
```math
j_A = - \rho \mathscr{D}_{AB} \tfrac{\partial \omega_a}{\partial x}
```
Unités :
```math
[\tfrac{kg}{m^2 \cdot s}] = [\tfrac{kg}{m^3}]\cdot[\tfrac{m^2}{s}]\cdot[\tfrac{kg}{kg}]
```
Forme générale :
```math
j_A = - \rho \mathscr{D}_{AB} \nabla \omega_a
```


## Notes de cours

Pour consulter les notes de cours, il suffit de cliquer
[ici](https://gitlab.com/marckit-poly/GCH3515-Notes-de-cours).

## Exercices

Afin de résoudre les problèmes où il y a un phénomène d'échange,
il suffit de suivre une [recette](#recette) de huit étapes
énoncés dans la sous-section suivante.

Pour consulter les exercices que j'ai effectuées, il suffit de cliquer
[ici](https://gitlab.com/marckit-poly/GCH3515-Exercices).

### Recette

Les étapes de la recette sont les suivantes :
1. Définition et hypotèses
2. Volume de contrôle
3. Tableau
4. Bilan
5. Équation
6. Condition frontières
7. Résolution
8. Propriétés

Les étapes les plus difficiles sont 1, 3 et 6. Le reste, c'est de l'algèbre.

## TD

Les TD remis ce trouvent dans le [dépôt suivant](
https://gitlab.com/marckit-poly/GCH3515-TD). Il n'est pas possible de consulter 
ce dossier à moins d'y participer.

## Feuilles de notes

Pour consulter les feuilles pour les examens, il faut aller dans les
notes de cours, il suffit de cliquer
[ici](https://gitlab.com/marckit-poly/GCH3515-Notes-de-cours).

